Array.prototype.mapToUnique = function () {
    let arrayCopy = [];
    this.forEach(v => arrayCopy.includes(v) ? false : arrayCopy.push(v));
    return arrayCopy;
}

Array.prototype.shuffle = function () {
    let arrayCopy = [...this];
    let newArray = [];
    for (let i = 0; i < this.length; i++) {
        let randomIndex = Math.floor(Math.random() * arrayCopy.length);
        newArray[i] = arrayCopy[randomIndex];
        arrayCopy = arrayCopy.filter((value, index) => index !== randomIndex);
    }
    return newArray;
}

Array.prototype.getRandomIndex = function() {
    return Math.floor(Math.random() * this.length);
}

Array.prototype.getRandomValue = function() {
    return this[this.getRandomIndex()];
}

Array.getDifference = (a, b, leftRight = 0) => [
    [...a.filter(value => !b.includes(value))],
    [...b.filter(value => !a.includes(value))]
]
    .filter((v, i) => (leftRight === -1) ? i === 0 : (leftRight === 1) ? i === 1 : true)
    .flat()
    .mapToUnique();

export default Array;