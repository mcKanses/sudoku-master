import Array from './Array.js';

const polyfills = {
    Array
}

export { Array };
export default polyfills;