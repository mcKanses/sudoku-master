import Sudoku from "./Sudoku.js";
import { Array } from "../polyfills/index.js";

const selectRandomCells = (sudoku, count) => {
  const valuesCopy = [...sudoku.getValues()];

  const selectedCells = [];

  let counter = 0;
  while (selectedCells.length < count) {
    const randomRow = Math.floor(Math.random() * 9);
    const randomCol = Math.floor(Math.random() * 9);

    if (selectedCells.filter((cell) => cell.row === randomRow && cell.col === randomCol).length > 0) continue;

    selectedCells[counter] = {
      row: randomRow,
      col: randomCol,
      value: valuesCopy[randomRow][randomCol],
    };

    // valuesCopy[randomRow] = valuesCopy[randomRow].filter((value, index) => index !== randomCol);

    counter++;
  }

  return selectedCells;
}

const fillCellElements = (selectedCells, cellElements) => {
  for (let i = 0; i < selectedCells.length; i++) {
    const prefilledCell = Array.from(cellElements)[selectedCells[i].row * 9 + selectedCells[i].col];
    prefilledCell.value = selectedCells[i].value;
    prefilledCell.setAttribute("readonly", "true");
    prefilledCell.classList.add("prefilled");
  }
}

const hasUniqueSolution = cells => {
      const solutionMatrix = Sudoku.createEmptyMatrix();
      cells.forEach((cell) => (solutionMatrix[cell.row][cell.col] = cell.value));

      let foundAtLeastOneUnique, countFilledCells;
      while ((countFilledCells = solutionMatrix.flat().filter((value) => value !== null).length) < 81) {
        foundAtLeastOneUnique = false;
        for (let row = 0; row < 9; row++) {
          for (let col = 0; col < 9; col++) {
            if (!!solutionMatrix[row][col]) continue;
            let solutions = [];
            let value = 1;
            for (; value < 10; value++) {
              const rowValues = solutionMatrix.filter((rows, rowIndex) => row === rowIndex)[0].filter(v => v !== null);
              const colValues = solutionMatrix.map(rows => rows.filter((v, colIndex) => colIndex === col)[0]).filter(v => v !== null);
              const subMatrixValues = solutionMatrix
                .filter((rows, rowIndex) => Math.floor(rowIndex / 3) === Math.floor(row / 3))
                .map(row => row.filter((cols, colIndex) => Math.floor(colIndex / 3) === Math.floor(col / 3)))
                .flat()
                .filter(value => value !== null);
              const allValues = [ ...rowValues, ...colValues, ...subMatrixValues ];
              if (!(allValues.includes(value))) solutions.push(value);
            }
            if (solutions.length === 0) alert();
            if (solutions.length === 1) {
              solutionMatrix[row][col] = solutions[0];
              foundAtLeastOneUnique = true;
            }
            // if (row === 8 && col === 8) debugger;
          }
        }
  
        if (!foundAtLeastOneUnique) break;
      }
      return countFilledCells >= 81;
      // }
}
class Game {
  getLevel() {
    return this.level;
  }
  setLevel(level) {
    this.level = level < 0 ? 0 : level > 2 ? 2 : level;
  }

  getCellElements() {
    return this.cellElements;
  }
  setCellElements(cellElements) {
    this.cellElements = cellElements;
  }

  getSudoku() {
    return this.sudoku;
  }

  start() {
    this.sudoku = new Sudoku();
    let selectedCells = selectRandomCells(this.sudoku, 37 - this.getLevel() * 10);
    while(!hasUniqueSolution(selectedCells)) { selectedCells = selectRandomCells(this.sudoku, 37 - this.getLevel() * 10); }
    fillCellElements(selectedCells, this.getCellElements());
  }

  reset = () =>
    Array.from(this.cellElements)
      .filter((cellElement) => !cellElement.classList.contains("prefilled"))
      .forEach((cellElement) => (cellElement.value = ""));
}

export default Game;
