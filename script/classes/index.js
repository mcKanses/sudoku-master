import Sudoku from './Sudoku.js';
import Cell from './Cell.js';
import Game from  './Game.js';

const classes = {
    Sudoku,
    Cell,
    Game
}

export { Sudoku, Cell, Game };
export default classes;