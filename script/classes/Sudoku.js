import { bindFunctions } from "../helpers/index.js";
import Cell from "./Cell.js";

const valuePool = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const createEmptyMatrix = () => (new Array(9)).fill([]).map((row) => (new Array(9)).fill(null));

let generateCells = function () {
  generateCell.call(this, 0, 0); // initial cell generation
}

let generateCell = function(x = 0, y = 0, value = null) {
  if (y > 8) return;
  const matrix = this.matrix;
  const cell = matrix[y][x] !== null && matrix[y][x] instanceof Cell
    ? matrix[y][x]
    : new Cell(x, y);
  const generatedValue = generateCellValue.call(this, cell);

  if (!generatedValue || generatedValue === null) {
    matrix[y][x] = null;
    const prevCell = getPreviousCell.call(this, cell);
    prevCell.forbid(prevCell.value);
    prevCell.setValue(null);
    return generateCell.call(this, prevCell.x, prevCell.y);
  };
  cell.setValue(generatedValue);
  matrix[y][x] = cell;
  generateCell.call(this, x < 8 ? x + 1 : 0, x < 8 ? y : y + 1);
}

let generateCellValue = function (cell) {
  
  const cellNeighbours = this.getNeighbours(cell.x, cell.y);
  const { row, col, subMatrix } = cellNeighbours;

  const forbidden = [...cell.forbidden, ...col.map((cell) => cell?.value), ...row.map((cell) => cell?.value), ...subMatrix.flat().map((cell) => cell?.value)]
    .flat()
    .mapToUnique()
    .filter((value) => !!value);
  const allowed = Array.getDifference(valuePool, forbidden, -1);
  let mustUseValues = [];

  if (cell.x > 2 && cell.x < 7 && cell.y % 3 > 0) {
    const nextSubMatrix = this.getSubMatrix(this.getSubMatrixIndex(cell.x, cell.y) + 1);
    mustUseValues = Array.getDifference(
      nextSubMatrix.flat().filter(value => !!value).map(cell => cell.value),
      forbidden, -1);
  }


  forbidden.forEach((value) => cell.forbid(value));

  let generatedValue = null;

  if (forbidden.length < 9) {
    let valuePool = allowed;
    if (cell.x > 2 && cell.x < 7 && cell.y % 2 > 0) {
      if (3 - (cell.x % 3) <= mustUseValues.length) valuePool = mustUseValues;
    }
    let randomValue = valuePool.length > 1 ? valuePool[Math.floor(Math.random() * valuePool.length)] : valuePool[0];
    generatedValue = randomValue;
  }

  cell.setValue(generatedValue);
  if (valuePool.includes(generatedValue)) cell.forbid(generatedValue);

  return generatedValue;
};

const calculateSubmatrixPosition = (col, row) => ({
  col: Math.floor(col / 3),
  row: Math.floor(row / 3),
});

const calculateSubmatrixIndex = (col, row) => calculateSubmatrixPosition(col, row).row * 3 + calculateSubmatrixPosition(col, row).col;

const calculatePositionInSubMatrix = (col, row) => ({
  col: col % 3,
  row: row % 3,
});

let getPreviousCell = function (cell) {
  const x = cell.x > 0 ? cell.x - 1 : 8;
  const y = cell.x > 0 ? cell.y : cell.y - 1;
  if (y < 0 || x < 0) return null;
  return this.matrix[y][x];
}

class Sudoku {
  constructor() {
    this.matrix = createEmptyMatrix();
    generateCells.call(this);
  }

  getValues = () => this.matrix.map((row, rowIndex) => row.map((cell) => cell !== null ? cell.getValue() : cell));

  getMatrix = () => this.matrix;

  getRow = (rowIndex) => [...this.getMatrix()[rowIndex]];

  getColumn = (col) =>
    this.getMatrix()
      .map((row) => row[col])
      .flat();

  getSubset = (colStart = 0, rowStart = 0, colEnd = 8, rowEnd = 8) =>
    this.getMatrix()
      .map((row) => row.filter((v, i) => i >= colStart && i <= colEnd))
      .filter((row, i) => i >= rowStart && i <= rowEnd);

  // getSubMatrix = (columnOrIndex, row = null) => (((row !== null || !row === undefined) && columnOrIndex > 2) || columnOrIndex > 8 || row > 2 ? null : this.getSubset((row !== null ? columnOrIndex % 3 : columnOrIndex) * 3, (row !== null ? row : Math.floor(columnOrIndex / 3)) * 3, ((row !== null ? columnOrIndex : columnOrIndex % 3) + 1) * 3 - 1, ((row !== null ? row : Math.floor(columnOrIndex / 3)) + 1) * 3 - 1));

  getSubMatrix = (columnOrIndex, row = null) => (row !== null ? this.getSubset(columnOrIndex * 3, row * 3, ++columnOrIndex * 3 - 1, (row + 1) * 3 - 1) : this.getSubset((columnOrIndex % 3) * 3, Math.floor(columnOrIndex / 3) * 3, ((columnOrIndex % 3) + 1) * 3 - 1, (Math.floor(columnOrIndex / 3) + 1) * 3 - 1));

  getPositionInSubMatrix = calculatePositionInSubMatrix;

  getSubMatrixPosition = calculateSubmatrixPosition;

  getSubMatrixIndex = calculateSubmatrixIndex;

  getNeighbours = (colIndex, rowIndex) => ({
    row: this.getRow(rowIndex).filter((v, i) => i !== colIndex),
    col: this.getColumn(colIndex).filter((v, i) => i !== rowIndex),
    subMatrix: this.getSubMatrix(this.getSubMatrixIndex(colIndex, rowIndex)).map((row, ri) => row.filter((cell, ci) => !(colIndex % 3 === ci && rowIndex % 3 === ri))),
  });

  checkSolution = values => values.flat().toString() === this.getValues().flat().toString();
}

Sudoku.createEmptyMatrix = createEmptyMatrix;
Sudoku.calculateSubmatrixIndex = calculateSubmatrixIndex;

export default Sudoku;
