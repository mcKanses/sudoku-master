import { Array } from "../polyfills/index.js";

const addForbiddenValue = (value, array) => (array = [...array, value]);

class Cell {
  constructor(x, y) {
    const instance = this;
    this.x = x;
    this.y = y;
    this.value = null;
    this.forbiddenValues = [];

    const returnObj = {

      forbiddenValues: [...instance.forbiddenValues],

      get x() { return this.getX(); },
      get y() { return this.getY(); },
      get value() { return this.getValue(); },
      get forbiddenValues() { return this.getForbiddenValues(); },
      get forbidden() { return this.getForbiddenValues(); },
      set value(v) { this.setValue(v); },

      getX: () => instance.x,
      getY: () => instance.y,
      getValue: () => instance.value,
      setValue(value) {
        !(this.getForbiddenValues().includes(value)) ? (instance.value = value) : (instance.value = instance.value);
        return instance.value;
      },
      getForbiddenValues: () => [...instance.forbiddenValues],

      forbid: (value) => !instance.forbiddenValues.includes(value) ? instance.forbiddenValues.push(value) : false,
    };

    Object.setPrototypeOf(returnObj, Cell.prototype);

    return Object.freeze(returnObj);
  }
}

export default Cell;
