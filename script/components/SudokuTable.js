const element = document.querySelector('#tbl-sudoku');

const clear = () => Array.from(element.querySelectorAll('td input')).forEach(cell => {
  cell.value = '';
  cell.classList.remove('prefilled');
  cell.removeAttribute('readonly');
});

const sudokuTable = {
  clear: clear
}

Object.freeze(sudokuTable);

export default sudokuTable;