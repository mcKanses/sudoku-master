import { Game, Sudoku } from "./classes/index.js";
import sudokuTable from "./components/SudokuTable.js";

const mainWrapper = document.querySelector("#main-wrapper");
const inputSelectLevel = document.querySelector("#sel-level");
const btnStartGame = document.querySelector("#btn-start-game");
const sudokuCells = document.querySelectorAll("#tbl-sudoku tr td input");

const game = new Game();
let timestamp;

inputSelectLevel.addEventListener("change", (ev) => {
  const levels = ["Leicht", "Mittel", "Schwer"];
  document.querySelector("#level-info").innerHTML = levels[inputSelectLevel.value];
});

btnStartGame.addEventListener("click", (ev) => {
  sudokuTable.clear();
  mainWrapper.style.marginLeft = "-100vw";
  mainWrapper.ontransitionend = () => {
    game.setLevel(document.querySelector("#sel-level").value);
    game.setCellElements(sudokuCells);
    game.start();

    timestamp = Date.now();

    Array.from(sudokuCells).forEach((cell) => {
      cell.addEventListener("focus", (ev) => ev.target.select());
      cell.addEventListener("keydown", (ev) => ((ev.keyCode < 49 || ev.keyCode > 59) && ev.keyCode !== 46 && ev.keyCode !== 8 ? ev.preventDefault() : true));

      cell.addEventListener("input", (ev) => {
        ev.target.select();
        btnReset.removeAttribute("disabled");
        const solved = game.getSudoku().checkSolution(Array.from(sudokuCells).map((cell) => cell.value));
        if (solved) alert('Du hast gewonnen!');
        // console.log(game);
			});
			
    });

    const timerHandleID = setInterval(() => {
      const elapsedTime = new Date(Date.now() - timestamp);
      document.querySelector("#timer").innerHTML = `${(elapsedTime.getHours() - 1).toString().padStart(2, "0")}:${elapsedTime.getMinutes().toString().padStart(2, "0")}:${elapsedTime.getSeconds().toString().padStart(2, "0")}`;
		}, 500);
  };

  const btnReset = document.querySelector("#btn-reset-sudoku");
  btnReset.addEventListener("click", (ev) => (confirm("Alle Werte werden gelöscht.\nFortfahren?") ? btnReset.setAttribute("disabled", "disabled") || game.reset() : false));

  const btnRestart = document.querySelector("#btn-new-sudoku");
  btnRestart.addEventListener("click", (ev) => {
    if (confirm("Aufgeben und neues Spiel starten?")) {
      document.querySelectorAll("td input").forEach((input) => {
        input.value = '';
        input.removeAttribute("readonly");
        input.removeAttribute("class");
      });
      game.start(true);
      timestamp = Date.now();
    }
  });
  const linkBackToStart = document.querySelector("#link-back-to-start");
  linkBackToStart.addEventListener("click", (ev) => (mainWrapper.style.marginLeft = "0"));
});
