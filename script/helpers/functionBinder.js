const bindFunctions = (obj, ...functions) =>
    functions.forEach(func => {
        func = func.bind(obj);
    });

export default bindFunctions;