import bindFunctions from './functionBinder.js';

const helpers = {
    bindFunctions
};

export { bindFunctions };
export default helpers;